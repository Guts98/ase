﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    public partial class SyntaxHelpForm : Form
    {
        public SyntaxHelpForm()
        {
            InitializeComponent();
            supportedColorListLabel.Text = "RED, BLUE, GREEN, BLACK, WHITE, ORANGE, PINK, PURPLE, YELLOW";
            hexColorInfoLabel.Text = "(Hex Color Codes are also valid!)";
            fillValuesLabel.Text = "Only 'On' or 'Off' values are allowed.";
        }


        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gplSyntaxDBDataSet.gplSyntaxes' table. You can move, or remove it, as needed.
            this.gplSyntaxesTableAdapter.Fill(this.gplSyntaxDBDataSet.gplSyntaxes);
            StyleDataGridView();

        }

        public void StyleDataGridView()
        {

            gplSyntaxesDataGridView.BorderStyle = BorderStyle.None;
            gplSyntaxesDataGridView.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            gplSyntaxesDataGridView.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            gplSyntaxesDataGridView.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            gplSyntaxesDataGridView.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            gplSyntaxesDataGridView.BackgroundColor = Color.FromArgb(30, 30, 30);
           // gplSyntaxesDataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;//optional
            gplSyntaxesDataGridView.EnableHeadersVisualStyles = false;
            gplSyntaxesDataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
           // gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle.Font = new Font("Monsterrat", 10);
            gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(37, 37, 38);
            gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }
    }
}
