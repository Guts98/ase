﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    public partial class GPLForm : Form
    {
        //bitmap to be displayed on the output picture box
        Bitmap outputBitmap = new Bitmap(740, 400);         //760, 480 //743,402

        //bitmap to display cursor point also to be displayed on the output picturebox
        Bitmap cursorBitmap = new Bitmap(740, 400);

        //Canvas reference to draw on the bitmap graphics
        Canvas outputCanvas;

        //Parser reference to parse the commands to call approriate shape methods
        Parser parser;

        //Checks the syntax of the commands
        SyntaxCheck syntaxCheck;

        //bool value to track the parsing error
        //public bool parseError = false;

        //Stores all parse exceptions during execution of the command
        public StringBuilder parseExceptions;

        //number of errors encountered while parsing
        public int errorCount;

        //Checks if multicommandline was requested to run
        public static bool multiCommandRun;

        //Stores the path of the file currently loaded
        String loadedfilePath;

        //Stores the name of the file currently loaded
        String loadedFileName;

        //Default constructor for the GPLForm class
        public GPLForm()
        {
            InitializeComponent();
            outputCanvas = new Canvas(Graphics.FromImage(outputBitmap));  //class for handling the drawing, pass the drawing surface to it
            updateCursor(Graphics.FromImage(cursorBitmap));               //updates the cursor position of the canvas
            
            //initially no request is made to run multi command lines
            multiCommandRun = false;

            //by default no file's been loaded
            this.loadedfilePath = "";                                          
            this.loadedFileName = "";

            //set pen indicator
            penColorIndicator.Text = Convert.ToString(outputCanvas.pen.Width);

            //set brush indicator
            if (outputCanvas.fillStatus)
                brushColorIndicator.Text = "on";
            else
                brushColorIndicator.Text = "Off";
        }

        public void refreshPenBrushIndicators()
        {
            //set pen indicator
            penColorIndicator.Text = Convert.ToString(outputCanvas.pen.Width);

            //set brush indicator
            if(outputCanvas.fillStatus)
                brushColorIndicator.Text = "on";
            else
                brushColorIndicator.Text = "Off";

            //updateColorIndicators
            outputCanvas.updateColorIndicators();
            
        }

        //updates the cursor pointer on the cursor bitmap's graphics to the current coordinates in output canvas object
        public void updateCursor(Graphics graphics)
        {
            int x = outputCanvas.XPos;
            int y = outputCanvas.YPos;
            graphics.Clear(Color.Transparent);                           //clears and makes a fresh transparent bitmap off cursor bitmap graphics
            SolidBrush sb = new SolidBrush(Color.Red);                   //solidbrush to draw cursor circle
            //graphics.DrawEllipse(outputCanvas.pen, outputCanvas.XPos-5, outputCanvas.YPos-5, 9, 9);   //hollow cursor

            if (!(x == 0 && y == 0))
            {
                //decreases the starting point by half to center the circular point with the lines drawn
                x -= 6;                                                        
                y -= 6;
            }

            graphics.FillEllipse(sb, x, y, 11, 11);                        //Draws Filled Ellipse at the current position set in canvas
        }

        //action listener when the form loads initially
        private void GPLForm_Load(object sender, EventArgs e)
        {
            //commandLine.TabIndex = 0;
            //commandLine.Focus();
        }

        //action listener for when exit menu item is clicked
        private void exitMenu_Click(object sender, EventArgs e)
        {
            this.Close();                                                   //closes the form window
        }

        //action listener when enter is pressed while inside the single command line
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)                                     //if enter key is pressed
            {
                executeRun();
            }
        }

        //action listener when the run button is clicked
        private void runBtn_Click(object sender, EventArgs e)
        {
            executeRun();
        }

        //executes run method by differentiating whether it's for a single or multiline command
        public void executeRun()
        {
            String command = commandLine.Text.Trim().ToLower();          //read commandline, get rid of trailing spaces and convert to lower case
            errorCount = 0;                                               //reset error count for every new single line program
            parseExceptions = new StringBuilder();
            run(command, 1);                                                          //calls the run function to execute command
            if (multiCommandRun)
            {
                errorCount = 0;                                                     //reset error count for every new multi line program
                int lineNo = 1;                                                     //set the line no to 1
                parseExceptions = new StringBuilder();
                foreach (String line in multiCommandLine.Lines)
                {
                    String commandStatement = line.Trim().ToLower();          //read commandline, get rid of trailing spaces and convert to lower case
                    run(commandStatement, lineNo);
                    lineNo++;
                }
                multiCommandRun = false;
            }
            updateErrorDisplay();                                               //Updates the text shown in the error display text box
        }

        //executes syntax check method by differentiating whether it's for a single or multiline command
        public void executeSyntaxCheck()
        {
            if (multiCommandLine.Text != "")
                multiCommandRun = true;
            String command = commandLine.Text.Trim().ToLower();          //read commandline, get rid of trailing spaces and convert to lower case
            errorCount = 0;                                                //reset error count for every new single line program
            parseExceptions = new StringBuilder();
            syntaxChecker(command, 1);                                                          //calls the run function to execute command
            if (multiCommandRun)
            {
                errorCount = 0;                                                     //reset error count for every new multi line program
                int lineNo = 1;                                                     //set the line no to 1
                parseExceptions = new StringBuilder();
                foreach (String line in multiCommandLine.Lines)
                {
                    String commandStatement = line.Trim().ToLower();          //read commandline, get rid of trailing spaces and convert to lower case
                    syntaxChecker(commandStatement, lineNo);            
                    lineNo++;
                }
                multiCommandRun = false;
            }
            updateErrorDisplay();                                               //Updates the text shown in the error display text box
        }

        //run method that runs the commands from the command line by parsing and executing it
        public void run(String command, int lineNo)
        {
            ShapeFactory factory = new ShapeFactory();
            ArrayList shapes = new ArrayList();
            parser = new Parser();
            try
            {
                parser.parseCommand(command, outputCanvas,  lineNo, factory, shapes);                 //parses and separates the command and parameters from command line
                Shape s = (Shape)shapes[0];
                s.draw(outputCanvas.graphics);
            }
            catch(InvalidCommandException e)                                        //when command name is invalid
            {
                errorCount++;
                parseExceptions.Append(e.Message+"\n");
            }
            catch (InvalidSyntaxException e)                                            //when the overall syntax is wrong
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (ParameterFormatException e)                                          //when the format of the parameter is wrong
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidParameterLengthException e)                       //when the length of parameters is wrong for right command
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch(InvalidPenSizeException e)                                    //then the size of the pen is not within given range
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidColorException e)                                    //when the color specified is not valid
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            
            updateCursor(Graphics.FromImage(cursorBitmap));             //updates the cursor position

            refreshPenBrushIndicators();                                //refresh the brush and pen indicators

            errorDisplay.Text = parseExceptions.ToString();
            Refresh();                                                  //signify that displayneeds updating
        }

        //Checks the syntax of each commands in the program
        public void syntaxChecker(String command, int lineNo)                   //when command name is invalid
        {
            syntaxCheck = new SyntaxCheck();
            try{
                syntaxCheck.checkCommand(command, lineNo);                 //parses and separates the command and parameters from command line
            }
            catch (InvalidCommandException e)
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidSyntaxException e)                                     //when the overall syntax is wrong
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (ParameterFormatException e)                                      //when the format of the parameter is wrong
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidParameterLengthException e)                       //when the length of parameters is wrong for right command
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidPenSizeException e)                                    //then the size of the pen is not within given range
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }
            catch (InvalidColorException e)                                    //when the color specified is not valid
            {
                errorCount++;
                parseExceptions.Append(e.Message + "\n");
            }

            updateCursor(Graphics.FromImage(cursorBitmap));             //updates the cursor position

            errorDisplay.Text = parseExceptions.ToString();
            Refresh();
        }

        //Updates the text shown in the error display text box
        public void updateErrorDisplay()
        {
            if (errorCount == 0)
            {
                errorDisplay.Text = "No Errors Found";               //if parsed successfully, add no error found message to error display
                //sets font color of error display to green if no error found
                errorDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(222)))), ((int)(((byte)(111)))));
                errorHeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(222)))), ((int)(((byte)(111)))));
            }
            else
            {
                //sets font color of error display to red if error found
                errorDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(60)))), ((int)(((byte)(26)))));
                errorHeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(146)))), ((int)(((byte)(51)))));
            }
            errorHeaderLabel.Text = errorCount + " Errors";
        }

       

        //Paints action to be performed on output screen picture box
        private void outputScreen_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;                                    //get graphics context of form (which is being displayed)

            g.DrawImageUnscaled(outputBitmap, 0, 0);                    //put the off screen bitmap on the picture box
            g.DrawImageUnscaled(cursorBitmap, 0, 0);                    //put the cursor bitmap on the picture box
        }

        private void syntaxBtn_Click(object sender, EventArgs e)
        {
            executeSyntaxCheck();
        }

        private void loadMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "d:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                        multiCommandLine.Text = fileContent;
                        fileNameLabel.Text = Path.GetFileName(openFileDialog.FileName);     //display the file name to the file name label

                        //store the file's name and path to the object's internal instance variables
                        this.loadedFileName = Path.GetFileName(openFileDialog.FileName); ;
                        this.loadedfilePath = openFileDialog.FileName;
                    }
                }
            }
        }


       
        //Writes the contents of multi command line to the file bieing saved
        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            //Stream myStream;
            StreamWriter fWriter;                                                   //streamwriter to write to the file
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();                  //open save file dialog

            saveFileDialog1.Title = "Save Gpl Text Files";                            //save dialog title extension
            saveFileDialog1.DefaultExt = ".gpl.txt";                                         

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;

            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //write the contents of multi command line text box to file being saved
                fWriter = File.CreateText(saveFileDialog1.FileName);
                fWriter.Write(multiCommandLine.Text);
                fWriter.Close();

                //Sets the file name to the file name label
                if (!fileNameLabel.Text.Equals(Path.GetFileName(saveFileDialog1.FileName)))
                {
                    fileNameLabel.Text = Path.GetFileName(saveFileDialog1.FileName);           //display the file name to the file name label

                    //store the file's name and path to the object's internal instance variables
                    this.loadedFileName = Path.GetFileName(saveFileDialog1.FileName); 
                    this.loadedfilePath = saveFileDialog1.FileName;
                }
            }
           
        }


        //When the close button is clicked to close the opening file and delete its contents from multi command line
        private void fileCloseBtn_Click(object sender, EventArgs e)
        {
            if (!fileNameLabel.Equals("Untitled.gpl.txt"))
            {
                fileNameLabel.Text = "Untitled.gpl.txt";
                multiCommandLine.Text = "";
                this.loadedfilePath = "";
                this.loadedFileName = "";
            }
        }

        //Decides whether to add editing symbol i.e. * to the end of the file name label if a file is being loaded
        private void multiCommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (!this.loadedfilePath.Equals(""))
            {
                // Stores the file's content to a variable
                string fileText = File.ReadAllText(this.loadedfilePath);

                //Compare the cotent of multiline command and file's content
                if (!fileText.Equals(multiCommandLine.Text))
                    this.fileNameLabel.Text = this.loadedFileName + "*";
                else
                    fileNameLabel.Text = this.loadedFileName;
            }
        }







        //changes font color of file menu when mouse enters
        private void fileMenu_MouseEnter(object sender, EventArgs e)
        {
            fileMenu.ForeColor = Color.Black;
        }

        //changes font color of file menu when mouse leaves
        private void fileMenu_MouseLeave(object sender, EventArgs e)
        {
            fileMenu.ForeColor = Color.WhiteSmoke;
        }

        //changes font color of help menu when mouse enters
        private void helpMenu_MouseEnter(object sender, EventArgs e)
        {
            helpMenu.ForeColor = Color.Black;
        }

        //changes font color of help menu when mouse leaves
        private void helpMenu_MouseLeave(object sender, EventArgs e)
        {
            helpMenu.ForeColor = Color.WhiteSmoke;
        }

        //Opens syntax glossary i.e. a new form showing all the valid commands
        private void syntaxGlossaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SyntaxHelpForm helpForm = new SyntaxHelpForm();
            helpForm.ShowDialog();
        }
    }
}
