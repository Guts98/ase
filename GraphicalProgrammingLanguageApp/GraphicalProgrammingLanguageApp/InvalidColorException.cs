﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    [Serializable]
    class InvalidColorException : System.Exception
    {
        public InvalidColorException() : base() { }
        public InvalidColorException(int message) : base(String.Format("Invalid Color Exception at Line: {0}", message)) { }
        public InvalidColorException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected InvalidColorException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
