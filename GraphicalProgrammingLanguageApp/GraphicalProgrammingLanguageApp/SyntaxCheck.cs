﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    class SyntaxCheck
    {
        //tracks whether an error was encountered while parsing the commands
        private bool errorFound;

        //sets and gets the errorfound
        public bool ErrorFound { get => errorFound; set => errorFound = value; }

        //default constructor
        public SyntaxCheck()
        {
            this.errorFound = false;
        }

        //parses the string line written on the command lines and deduces and executes the underlying commands
        public void checkCommand(String line, int lineNo)
        {
            //if the line is empty
            if (line.Equals(""))
                return;

            line = line.ToLower().Trim();                                                   //trimm the trailing whitespaces from the command
            String[] splitLine = line.Split(' ');                                           //split the command into the command name and its parameters
            String command = splitLine[0];                                                  //first part stored as command

            //If invalid command is typed
            if (!(command.Equals("drawto") || command.Equals("square") || command.Equals("rect") || command.Equals("circle") || command.Equals("moveto") ||
                command.Equals("triangle") || command.Equals("clear") || command.Equals("halftriangle") || command.Equals("run") || command.Equals("reset") ||
                command.Equals("pensize") || command.Equals("pencolor") || command.Equals("fill") || command.Equals("fillcolor")))
            {
                throw new InvalidCommandException(lineNo);                                  //when command name is invalid
            }


            if (splitLine.Length > 2)
            {
                throw new InvalidSyntaxException(lineNo);                                   //when the overall syntax is wrong
            }
            //if there exists the parameters along with the command
            else if (splitLine.Length > 1)
            {
                String[] parameters = splitLine[1].Split(',');                              //second part stored in array to be further split by comma
                int[] parametersInt = new int[parameters.Length];                           //int array for storing parameters

                //if parameters are not supposed to take string parameters
                if (!(command.Equals("pencolor") || command.Equals("fill") || command.Equals("fillcolor")))
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        try
                        {
                            parametersInt[i] = Convert.ToInt32(parameters[i].Trim());            //convert string parameters to int 
                        }
                        catch (FormatException)
                        {
                            throw new ParameterFormatException(lineNo);                           //when the format of the parameter is wrong
                        }
                    }
                }

                //Switch cases to call associated methods for different commands with parameters
                switch (command)
                {
                    case "pensize":                                                               //sets the width of the pen
                        Parser.checkParameterLength(1, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "pencolor":                                                             //set the color of the pen
                        Parser.checkStringParameterLength(1, parameters, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "fill":                                                             //set the color of the pen
                        Parser.checkStringParameterLength(1, parameters, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "fillcolor":                                                             //set the color of the pen
                        Parser.checkStringParameterLength(1, parameters, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "drawto":                                                                //draws Lilne on output canvas
                        Parser.checkParameterLength(2, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "square":                                                              //draws Square on output canvas
                        Parser.checkParameterLength(1, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "rect":                                                                //draws Rectangle on output canvas
                        Parser.checkParameterLength(2, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "circle":                                                              //draws Rectangle on output canvas
                        Parser.checkParameterLength(1, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "triangle":                                                              //draws Triangle on output canvas
                        Parser.checkParameterLength(2, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "halftriangle":                                                              //draws Triangle on output canvas
                        Parser.checkParameterLength(2, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "moveto":                                                              //move cursor position on output canvas
                        Parser.checkParameterLength(2, parametersInt, lineNo);                         //check parameter length before running the comamnd
                        break;
                    case "clear":                                                               //if clear command is typed but with parameters                                
                        throw new InvalidParameterLengthException(lineNo);                      //throw new wrong parameter number exception
                    case "run":                                                               //if clear command is typed but with parameters                                   
                        throw new InvalidParameterLengthException(lineNo);                  //throw new wrong parameter number exception
                    case "reset":                                                               //if reset command is typed but with parameters                                   
                        throw new InvalidParameterLengthException(lineNo);                  //throw new wrong parameter number exception
                    default:                                                                    //default
                        throw new InvalidCommandException(lineNo);                              //when command name is invalid
                }
            }

          


        }

    }
}
