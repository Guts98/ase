﻿namespace GraphicalProgrammingLanguageApp
{
    partial class SyntaxHelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.supportedColorHeaderLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gplSyntaxesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gplSyntaxDBDataSet = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSet();
            this.fillValuesLabel = new System.Windows.Forms.Label();
            this.hexColorInfoLabel = new System.Windows.Forms.Label();
            this.supportedColorListLabel = new System.Windows.Forms.Label();
            this.fillOptionsLabel = new System.Windows.Forms.Label();
            this.gplSyntaxesTableAdapter = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSetTableAdapters.gplSyntaxesTableAdapter();
            this.tableAdapterManager = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSetTableAdapters.TableAdapterManager();
            this.authorInfoLabel = new System.Windows.Forms.Label();
            this.gplSyntaxDBDataSet1 = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSet();
            this.gplSyntaxesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gplSyntaxesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Montserrat", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(198)))), ((int)(((byte)(35)))));
            this.label1.Location = new System.Drawing.Point(266, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(680, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Graphical Programming Language: Syntax Guide";
            this.label1.UseMnemonic = false;
            // 
            // supportedColorHeaderLabel
            // 
            this.supportedColorHeaderLabel.AutoSize = true;
            this.supportedColorHeaderLabel.Font = new System.Drawing.Font("Montserrat", 8.25F, System.Drawing.FontStyle.Bold);
            this.supportedColorHeaderLabel.ForeColor = System.Drawing.Color.SpringGreen;
            this.supportedColorHeaderLabel.Location = new System.Drawing.Point(320, 545);
            this.supportedColorHeaderLabel.Name = "supportedColorHeaderLabel";
            this.supportedColorHeaderLabel.Size = new System.Drawing.Size(152, 20);
            this.supportedColorHeaderLabel.TabIndex = 1;
            this.supportedColorHeaderLabel.Text = "Supported Colors:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InfoText;
            this.panel1.Controls.Add(this.gplSyntaxesDataGridView);
            this.panel1.Controls.Add(this.authorInfoLabel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.fillValuesLabel);
            this.panel1.Controls.Add(this.hexColorInfoLabel);
            this.panel1.Controls.Add(this.supportedColorListLabel);
            this.panel1.Controls.Add(this.fillOptionsLabel);
            this.panel1.Controls.Add(this.supportedColorHeaderLabel);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1174, 629);
            this.panel1.TabIndex = 2;
            // 
            // gplSyntaxesBindingSource
            // 
            this.gplSyntaxesBindingSource.DataMember = "gplSyntaxes";
            this.gplSyntaxesBindingSource.DataSource = this.gplSyntaxDBDataSet;
            // 
            // gplSyntaxDBDataSet
            // 
            this.gplSyntaxDBDataSet.DataSetName = "gplSyntaxDBDataSet";
            this.gplSyntaxDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fillValuesLabel
            // 
            this.fillValuesLabel.AutoSize = true;
            this.fillValuesLabel.Font = new System.Drawing.Font("Montserrat", 8.25F);
            this.fillValuesLabel.ForeColor = System.Drawing.Color.Snow;
            this.fillValuesLabel.Location = new System.Drawing.Point(428, 571);
            this.fillValuesLabel.Name = "fillValuesLabel";
            this.fillValuesLabel.Size = new System.Drawing.Size(259, 20);
            this.fillValuesLabel.TabIndex = 1;
            this.fillValuesLabel.Text = "Only \'On\' or \'Off\' values are allowed.";
            // 
            // hexColorInfoLabel
            // 
            this.hexColorInfoLabel.AutoSize = true;
            this.hexColorInfoLabel.Font = new System.Drawing.Font("Montserrat", 8.25F);
            this.hexColorInfoLabel.ForeColor = System.Drawing.Color.Snow;
            this.hexColorInfoLabel.Location = new System.Drawing.Point(684, 571);
            this.hexColorInfoLabel.Name = "hexColorInfoLabel";
            this.hexColorInfoLabel.Size = new System.Drawing.Size(233, 20);
            this.hexColorInfoLabel.TabIndex = 1;
            this.hexColorInfoLabel.Text = "(Hex Color Codes are also valid!)";
            // 
            // supportedColorListLabel
            // 
            this.supportedColorListLabel.AutoSize = true;
            this.supportedColorListLabel.Font = new System.Drawing.Font("Montserrat", 7F);
            this.supportedColorListLabel.ForeColor = System.Drawing.Color.Snow;
            this.supportedColorListLabel.Location = new System.Drawing.Point(475, 549);
            this.supportedColorListLabel.Name = "supportedColorListLabel";
            this.supportedColorListLabel.Size = new System.Drawing.Size(438, 16);
            this.supportedColorListLabel.TabIndex = 1;
            this.supportedColorListLabel.Text = "RED, BLUE, GREEN, BLACK, WHITE, ORANGE, PINK, PURPLE, YELLOW";
            // 
            // fillOptionsLabel
            // 
            this.fillOptionsLabel.AutoSize = true;
            this.fillOptionsLabel.Font = new System.Drawing.Font("Montserrat", 8.25F, System.Drawing.FontStyle.Bold);
            this.fillOptionsLabel.ForeColor = System.Drawing.Color.SpringGreen;
            this.fillOptionsLabel.Location = new System.Drawing.Point(320, 571);
            this.fillOptionsLabel.Name = "fillOptionsLabel";
            this.fillOptionsLabel.Size = new System.Drawing.Size(106, 20);
            this.fillOptionsLabel.TabIndex = 1;
            this.fillOptionsLabel.Text = "Fill Options:";
            // 
            // gplSyntaxesTableAdapter
            // 
            this.gplSyntaxesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.gplSyntaxesTableAdapter = this.gplSyntaxesTableAdapter;
            this.tableAdapterManager.UpdateOrder = GraphicalProgrammingLanguageApp.gplSyntaxDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // authorInfoLabel
            // 
            this.authorInfoLabel.AutoSize = true;
            this.authorInfoLabel.Font = new System.Drawing.Font("Montserrat", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authorInfoLabel.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.authorInfoLabel.Location = new System.Drawing.Point(355, 591);
            this.authorInfoLabel.Name = "authorInfoLabel";
            this.authorInfoLabel.Size = new System.Drawing.Size(522, 18);
            this.authorInfoLabel.TabIndex = 6;
            this.authorInfoLabel.Text = "Copyright @ 2020 | Amit Kumar Karn | Kathmandu, Nepal | All Rights Reserved ";
            // 
            // gplSyntaxDBDataSet1
            // 
            this.gplSyntaxDBDataSet1.DataSetName = "gplSyntaxDBDataSet";
            this.gplSyntaxDBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gplSyntaxesBindingSource1
            // 
            this.gplSyntaxesBindingSource1.DataMember = "gplSyntaxes";
            this.gplSyntaxesBindingSource1.DataSource = this.gplSyntaxDBDataSet1;
            // 
            // gplSyntaxesDataGridView
            // 
            this.gplSyntaxesDataGridView.AllowUserToAddRows = false;
            this.gplSyntaxesDataGridView.AllowUserToDeleteRows = false;
            this.gplSyntaxesDataGridView.AllowUserToOrderColumns = true;
            this.gplSyntaxesDataGridView.AutoGenerateColumns = false;
            this.gplSyntaxesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Montserrat", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gplSyntaxesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gplSyntaxesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.gplSyntaxesDataGridView.DataSource = this.gplSyntaxesBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Montserrat", 8F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gplSyntaxesDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.gplSyntaxesDataGridView.Location = new System.Drawing.Point(3, 38);
            this.gplSyntaxesDataGridView.Name = "gplSyntaxesDataGridView";
            this.gplSyntaxesDataGridView.ReadOnly = true;
            this.gplSyntaxesDataGridView.RowHeadersWidth = 51;
            this.gplSyntaxesDataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.gplSyntaxesDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Montserrat", 8F);
            this.gplSyntaxesDataGridView.RowTemplate.DividerHeight = 2;
            this.gplSyntaxesDataGridView.RowTemplate.Height = 35;
            this.gplSyntaxesDataGridView.RowTemplate.ReadOnly = true;
            this.gplSyntaxesDataGridView.Size = new System.Drawing.Size(1168, 504);
            this.gplSyntaxesDataGridView.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.FillWeight = 4F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Command";
            this.dataGridViewTextBoxColumn2.FillWeight = 9F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Command";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ParameterLength";
            this.dataGridViewTextBoxColumn3.FillWeight = 14F;
            this.dataGridViewTextBoxColumn3.HeaderText = "ParameterLength";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Parameters";
            this.dataGridViewTextBoxColumn4.FillWeight = 27F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Parameters";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn5.FillWeight = 46F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Description";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // SyntaxHelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(140)))), ((int)(((byte)(207)))));
            this.ClientSize = new System.Drawing.Size(1174, 629);
            this.Controls.Add(this.panel1);
            this.Name = "SyntaxHelpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GPL App: Syntax Help";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label supportedColorHeaderLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label supportedColorListLabel;
        private System.Windows.Forms.Label hexColorInfoLabel;
        private System.Windows.Forms.Label fillValuesLabel;
        private System.Windows.Forms.Label fillOptionsLabel;
        private gplSyntaxDBDataSet gplSyntaxDBDataSet;
        private System.Windows.Forms.BindingSource gplSyntaxesBindingSource;
        private gplSyntaxDBDataSetTableAdapters.gplSyntaxesTableAdapter gplSyntaxesTableAdapter;
        private gplSyntaxDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label authorInfoLabel;
        private gplSyntaxDBDataSet gplSyntaxDBDataSet1;
        private System.Windows.Forms.BindingSource gplSyntaxesBindingSource1;
        private System.Windows.Forms.DataGridView gplSyntaxesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    }
}