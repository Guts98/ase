﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    [Serializable]
    class ParameterFormatException: System.Exception
    {
        public ParameterFormatException() : base() { }
        public ParameterFormatException(int message) : base(String.Format("Invalid Parameter Exception at Line: {0}", message)) { }
        public ParameterFormatException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected ParameterFormatException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
