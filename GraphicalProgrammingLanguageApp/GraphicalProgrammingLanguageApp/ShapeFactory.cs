﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    class ShapeFactory
    {
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim();

            if(shapeType.Equals("SHAPERECTANGLE"))
            {
                return new Rectangle();
            }
            else
            {
                throw new InvalidCommandException(99);
            }
        }

    }
}
