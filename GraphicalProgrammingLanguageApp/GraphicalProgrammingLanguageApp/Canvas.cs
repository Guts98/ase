﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    class Canvas
    {
        //Graphics reference of the bitmap's graphics to draw onto
        public Graphics graphics;

        //Pen reference to pen object that draws on the bitmap's graphics
        public Pen pen;

        //x-coordinate of the bitmap
        private int xPos;

        //y-coordinate of the bitmap
        private int yPos;

        //sets and returns the value of x-coordinate
        public int XPos { get => xPos; set => xPos = value; }

        //sets and returns the value of y-coordinate
        public int YPos { get => yPos; set => yPos = value; }

        //Keep track whether to draw a shape with filled color or hollow
        public bool fillStatus;

        //Stores the color for filling the shapes
        public Color fillColor;

        //Solid brush to paint the color to the shapes
        public SolidBrush fillBrush;

        //Default Parameterized constructor taking graphics object of the bitmap to draw onto
        public Canvas(Graphics graphics)
        {
            this.graphics = graphics;                                                //assigns the reference to bitmap's graphics object
            this.xPos = this.yPos = 0;                                               //initial cursor position set to 0,0 
            pen = new Pen(Color.Black, 2);                                           //pen object to draw on the bitmap graphics

            this.fillColor = Color.Green;                                           //default fill color
            this.fillStatus = false;                                                 //by default fill is turned off
            this.fillBrush = new SolidBrush(this.fillColor);                         //default fill color for solid brush
            updateColorIndicators();
        }

       //Draws pen color and brush color indicators as circle on canvas
       public void updateColorIndicators()
        {
            graphics.FillEllipse(new SolidBrush(this.pen.Color), 718, 353, 15, 15);     //pen color indicator
            graphics.FillEllipse(this.fillBrush,718, 380, 15, 15);                      //fill color indicator
        }

        //draws a line from current position to provided arguments (toX, toY)
        public void drawTo(int toX, int toY)
        {
            graphics.DrawLine(pen, XPos, YPos, toX, toY);                           //draws line from current to provided coordinates
            //sets the cursur coordinates to the line at the end of the line
            this.xPos = toX;    
            this.yPos = toY;
        }

        //draws a square around the current position (xPos, yPos)
        public void drawSquare(int width)
        {
            //if fill is on
            if(this.fillStatus)
            {
                graphics.FillRectangle(this.fillBrush, (this.xPos - width / 2), (this.yPos - width / 2), width, width);
            }
            //draws a hollow square around the cursor pointer with the width provided
            else
                graphics.DrawRectangle(pen, (this.xPos - width / 2), (this.yPos - width / 2), width, width); 
        }

        //draws a rectangle around the current position (xPos, yPos)
        public void drawRectangle(int width, int height)
        {
            //if fill is on
            if (this.fillStatus)
            {
                graphics.FillRectangle(this.fillBrush, (this.xPos - width / 2), (this.yPos - height / 2), width, height);
            }
            //draws a rectangle around the cursor pointer with the width and height provided
            else
                graphics.DrawRectangle(pen, (this.xPos - width / 2), (this.yPos - height / 2), width, height);
        }

        //draws a circle around the current position (xPos, yPos)
        public void drawCircle(int radius)
        {
            //if fill is on
            if (this.fillStatus)
            {
                graphics.FillEllipse(this.fillBrush, (this.xPos - radius), (this.yPos - radius), (radius * 2), (radius * 2));
            }
            //draws a circle around the cursor pointer with the radius provided
            else
                graphics.DrawEllipse(pen, (this.xPos - radius) , (this.yPos - radius) , (radius*2), (radius*2));
        }

        //draws a triangle from the current position (xPos, yPos)
        public void drawTriangle(int width, int height)
        {
            //draws a triangle around the cursor pointer with the width and height provided
            int x = this.xPos - width / 2;
            int y = this.yPos + height / 2;
            graphics.DrawLine(pen, x, y, x + width, y);
            graphics.DrawLine(pen,x+width, y, this.xPos, this.yPos-height/2);
            graphics.DrawLine(pen, this.xPos, this.yPos - height / 2, x, y);
        }

        //draws a triangle from the current position (xPos, yPos)
        public void drawHalfTriangle(int width, int height)
        {
            //draws a triangle around the cursor pointer with the width and height provided
            int x = this.xPos - width / 2;
            int y = this.yPos + height / 2;
            graphics.DrawLine(pen, x, y, x + width, y);
            graphics.DrawLine(pen, x + width, y, this.xPos, this.yPos);
            graphics.DrawLine(pen, this.xPos, this.yPos, x, y);
        }

        //sets the cursor pointer to the coordinates provided
        public void moveTo(int x, int y)
        {
            //sets the coordinates to the provided values
            this.xPos = x;
            this.yPos = y;
        }

        //clears the bitmap's graphics of the picturebox
        public void clear()
        {
            //clears the output bitmap's graphics and refills it with white color
            graphics.Clear(Color.White);                                         
            //reset the cursor pointer to initial position i.e. (0,0)
            this.xPos = 0;
            this.yPos = 0;
        }

        //runs the commands on the multi line command box
        public void run(RichTextBox multiCommandLine)
        {
            if(multiCommandLine.Lines.Length>1)
            {
                multiCommandLine.Text = "Multiple Lines => Line 1: "+ multiCommandLine.Lines[0]+ "\n Line 2: " + multiCommandLine.Lines[1];
            }
            else
            {
                multiCommandLine.Text = "Single Lines";
            }
        }

        //sets pen color to passed color
        public void setPenColor(String color, int lineNo)
        {
            color = color.ToLower(); 
            //if hex value is recieved
            if (color[0].Equals('#'))
            {
                this.pen.Color = GetSolidColorBrush(color, lineNo);
                return;

            }

            //check if the color matches the limited list of color list
            if (!(color.Equals("red") || color.Equals("blue") || color.Equals("green") || color.Equals("black") || color.Equals("white") || color.Equals("orange") ||
                color.Equals("pink") || color.Equals("purple") || color.Equals("yellow")))
            {
                throw new InvalidColorException(lineNo);
            }
                
            String pColor = char.ToUpper(color[0]) + color.Substring(1);    //convert first letter capital
            this.pen.Color = Color.FromName(pColor);
        }

        //sets pen size to passed size
        public void setPenSize(int pSize, int lineNo)
        {
            //checks if the pen size is within a valid range and throws an exception if not
            if (pSize < 0 || pSize > 20)
                throw new InvalidPenSizeException(lineNo);
            this.pen.Width = pSize;
        }

        //sets fill color to passed color
        public void setFillColor(String color, int lineNo)
        {
            color = color.ToLower();

            //if hex value is recieved
            if(color[0].Equals('#'))
            {
                this.fillColor  = GetSolidColorBrush(color, lineNo);
                fillBrush = new SolidBrush(this.fillColor);
                return;
            }
            

            //check if the color matches the limited list of color list
            if (!(color.Equals("red") || color.Equals("blue") || color.Equals("green") || color.Equals("black") || color.Equals("white") || color.Equals("orange") ||
                color.Equals("pink") || color.Equals("purple") || color.Equals("yellow")))
            {
                throw new InvalidColorException(lineNo);
            }

            String pColor = char.ToUpper(color[0]) + color.Substring(1);        //convert first letter capital
            this.fillColor = Color.FromName(pColor);

            fillBrush = new SolidBrush(this.fillColor);
        }

        //Turns the fill on and off for the shapes
        public void setFillStatus(String fStatus, int lineNo)
        {
            if (fStatus.ToLower() == "on")
                this.fillStatus = true;
            else if (fStatus.ToLower() == "off")
                this.fillStatus = false;
            else
                throw new ParameterFormatException(lineNo);
        }

        //sets default pen  and solid brush
        public void reset()
        {
           // this.xPos = this.yPos = 0;                                               //initial cursor position set to 0,0 
            pen = new Pen(Color.Black, 2);                                           //pen object to draw on the bitmap graphics

            this.fillColor = Color.Black;                                           //default fill color
            this.fillStatus = false;                                                 //by default fill is turned off
            this.fillBrush = new SolidBrush(this.fillColor);                         //default fill color for solid brush

        }


        //converts hex coded color to rgb
        public Color GetSolidColorBrush(string hex, int lineNo)
        {
            Color color;
            hex = hex.Replace("#", string.Empty);
            byte[] argb = new byte[4];
            try
            {
                argb[0] = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
                argb[1] = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
                argb[2] = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
                argb[3] = 1;//(byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
                color = System.Drawing.Color.FromArgb(((int)(argb[0])), ((int)(argb[1])), ((int)((argb[2]))));
            }
            catch(InvalidColorException)
            {
                throw new InvalidColorException(lineNo);
            }
            catch(System.Exception)
            {
                throw new InvalidColorException(lineNo);
            }
            //SolidColorBrush myBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(a, r, g, b));
            return color;
        }
    }
}
