﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    [Serializable]
    class InvalidParameterLengthException : System.Exception
    {
        public InvalidParameterLengthException() : base() { }
        public InvalidParameterLengthException(int message) : base(String.Format("Wrong number of parameters at at Line: {0}", message)) { }
        public InvalidParameterLengthException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected InvalidParameterLengthException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
