﻿//https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/serialization/how-to-write-object-data-to-an-xml-file
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializeXML
{
    public class XMLWrite
    {
        const String filename = "\\SerializationOverview.xml";
        static void Main(string[] args)
        {
            Console.WriteLine("Writing XML");

            WriteXML();
            //once you have written the file, try editing it and then reading it in
            readXML();
            
            Console.ReadKey();
        }

        public class Book
        {
            public String title;
            public String author;
            public String ISBN;
            private int checksum=99;
            protected int something=99;
        }

        public static void WriteXML()// Object o, String fn)
        {
            Book overview = new Book();
            overview.title = "The Book Of Dust";
            overview.author = "Philip Pullman";
            overview.ISBN = "ISBN000111000";
            
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Book));

            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + filename;
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, (Book) overview);
            file.Close();
        }

        public static void readXML()
        {
            // Now we can read the serialized book ...  
            System.Xml.Serialization.XmlSerializer reader =
                new System.Xml.Serialization.XmlSerializer(typeof(Book));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + filename;
            System.IO.StreamReader file = new System.IO.StreamReader(
                path);
            Book overview = (Book)reader.Deserialize(file);
            file.Close();

            Console.WriteLine(overview.title);
            Console.WriteLine(overview.author);
            Console.WriteLine(overview.ISBN);
        }
    }
}
