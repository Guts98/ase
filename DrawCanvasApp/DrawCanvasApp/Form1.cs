﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawCanvasApp
{
    public partial class Form1 : Form
    {
        Graphics g;
        public Form1()
        {
            InitializeComponent();
            g = canvasPanel.CreateGraphics();
        }

        bool startPaint = false;

        //nullabel int for storing Null value
        int? initX = null;
        int? initY = null;
        bool drawSquare = false;
        bool drawRectangle = false;
        bool drawCircle = false;

       

        private void canvasPanel_MouseDown(object sender, MouseEventArgs e)
        {
            startPaint = true;
            if(drawSquare)
            {
                //Use Solid Brush for filling the graphics shapes
                SolidBrush sb = new SolidBrush(btnPenColor.BackColor);
                //setting the width and height same for creating square
                //Getting the width and height value from the TextBox TextBox(textShapeSize)
                g.FillRectangle(sb, e.X, e.Y, int.Parse(textShapeSize.Text), int.Parse(textShapeSize.Text));
                //setting startPaint and drawSquare value to falsefor creating one graphics on one click.
                startPaint = false;
                drawSquare = false;
            }
            if(drawRectangle)
            {
                SolidBrush sb = new SolidBrush(btnPenColor.BackColor);
                //setting the width of twice of the height
                g.FillRectangle(sb, e.X, e.Y, 2 * int.Parse(textShapeSize.Text), int.Parse(textShapeSize.Text));
                startPaint = false;
                drawRectangle = false;
            }
            if(drawCircle)
            {
                SolidBrush sb = new SolidBrush(btnPenColor.BackColor);
                g.FillEllipse(sb, e.X, e.Y, int.Parse(textShapeSize.Text), int.Parse(textShapeSize.Text));
                startPaint = false;
                drawCircle = false;
            }
        }

        private void canvasPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if(startPaint)
            {
                //Setting the Pen BackColor and line width
                //btnPenColor.BackColor = Color.Red;
                Pen p = new Pen(btnPenColor.BackColor, float.Parse(comboPenSize.Text));
                //Drawing the line
                g.DrawLine(p, new Point(initX ?? e.X, initY ?? e.Y), new Point(e.X, e.Y));
                initX = e.X;
                initY = e.Y;
            }
        }

        private void canvasPanel_MouseUp(object sender, MouseEventArgs e)
        {
            //dot
            Pen p = new Pen(btnPenColor.BackColor, float.Parse(comboPenSize.Text));
            //Drawing the line
            g.DrawLine(p, new Point(initX ?? e.X, initY ?? e.Y), new Point((initX ?? e.X+1), (initY ?? e.Y+1)));

            startPaint = false;
            initX = null;
            initY = null;
        }

        private void btnSetPen(object sender, EventArgs e)
        {
            //Open Color Dialog and Set BackColor of btnPenColor if user click on OK
            ColorDialog c = new ColorDialog();
            if(c.ShowDialog() == DialogResult.OK)
            {
                btnPenColor.BackColor = c.Color;
            }
        }

        private void btnPenColor_Click(object sender, EventArgs e)
        {

        }

        private void btnCanvasColor_Click(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();
            if(c.ShowDialog() == DialogResult.OK)
            {
                canvasPanel.BackColor = c.Color;
                btnCanvasColor.BackColor = c.Color;
            }
        }

        private void btnSquare_Click(object sender, EventArgs e)
        {
            drawSquare = true;
        }

        private void btnRectangle_Click(object sender, EventArgs e)
        {
            drawRectangle = true;
        }

        private void btnCircle_Click(object sender, EventArgs e)
        {
            drawCircle = true;
        }


    }
}
