﻿namespace DrawCanvasApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvasPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSquare = new System.Windows.Forms.Button();
            this.btnPenColor = new System.Windows.Forms.Button();
            this.btnRectangle = new System.Windows.Forms.Button();
            this.btnCanvasColor = new System.Windows.Forms.Button();
            this.btnCircle = new System.Windows.Forms.Button();
            this.comboPenSize = new System.Windows.Forms.ComboBox();
            this.textShapeSize = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // canvasPanel
            // 
            this.canvasPanel.BackColor = System.Drawing.SystemColors.Control;
            this.canvasPanel.Location = new System.Drawing.Point(77, 136);
            this.canvasPanel.Name = "canvasPanel";
            this.canvasPanel.Size = new System.Drawing.Size(928, 406);
            this.canvasPanel.TabIndex = 0;
            this.canvasPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvasPanel_MouseDown);
            this.canvasPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvasPanel_MouseMove);
            this.canvasPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvasPanel_MouseUp);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(77, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 55);
            this.button1.TabIndex = 1;
            this.button1.Text = "Set Pen";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btnSetPen);
            // 
            // btnSquare
            // 
            this.btnSquare.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSquare.FlatAppearance.BorderSize = 0;
            this.btnSquare.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSquare.Location = new System.Drawing.Point(77, 75);
            this.btnSquare.Name = "btnSquare";
            this.btnSquare.Size = new System.Drawing.Size(125, 55);
            this.btnSquare.TabIndex = 1;
            this.btnSquare.Text = "Square";
            this.btnSquare.UseVisualStyleBackColor = false;
            this.btnSquare.Click += new System.EventHandler(this.btnSquare_Click);
            // 
            // btnPenColor
            // 
            this.btnPenColor.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPenColor.FlatAppearance.BorderSize = 0;
            this.btnPenColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPenColor.Location = new System.Drawing.Point(345, 14);
            this.btnPenColor.Name = "btnPenColor";
            this.btnPenColor.Size = new System.Drawing.Size(125, 55);
            this.btnPenColor.TabIndex = 1;
            this.btnPenColor.Text = "Pen Color";
            this.btnPenColor.UseVisualStyleBackColor = false;
            this.btnPenColor.Click += new System.EventHandler(this.btnPenColor_Click);
            // 
            // btnRectangle
            // 
            this.btnRectangle.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRectangle.FlatAppearance.BorderSize = 0;
            this.btnRectangle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRectangle.Location = new System.Drawing.Point(345, 75);
            this.btnRectangle.Name = "btnRectangle";
            this.btnRectangle.Size = new System.Drawing.Size(125, 57);
            this.btnRectangle.TabIndex = 1;
            this.btnRectangle.Text = "Rectangle";
            this.btnRectangle.UseVisualStyleBackColor = false;
            this.btnRectangle.Click += new System.EventHandler(this.btnRectangle_Click);
            // 
            // btnCanvasColor
            // 
            this.btnCanvasColor.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCanvasColor.FlatAppearance.BorderSize = 0;
            this.btnCanvasColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCanvasColor.Location = new System.Drawing.Point(618, 12);
            this.btnCanvasColor.Name = "btnCanvasColor";
            this.btnCanvasColor.Size = new System.Drawing.Size(125, 55);
            this.btnCanvasColor.TabIndex = 1;
            this.btnCanvasColor.Text = "Canvas Color";
            this.btnCanvasColor.UseVisualStyleBackColor = false;
            this.btnCanvasColor.Click += new System.EventHandler(this.btnCanvasColor_Click);
            // 
            // btnCircle
            // 
            this.btnCircle.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCircle.FlatAppearance.BorderSize = 0;
            this.btnCircle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCircle.Location = new System.Drawing.Point(618, 73);
            this.btnCircle.Name = "btnCircle";
            this.btnCircle.Size = new System.Drawing.Size(125, 57);
            this.btnCircle.TabIndex = 1;
            this.btnCircle.Text = "btnCircle";
            this.btnCircle.UseVisualStyleBackColor = false;
            this.btnCircle.Click += new System.EventHandler(this.btnCircle_Click);
            // 
            // comboPenSize
            // 
            this.comboPenSize.FormattingEnabled = true;
            this.comboPenSize.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboPenSize.Location = new System.Drawing.Point(820, 28);
            this.comboPenSize.Name = "comboPenSize";
            this.comboPenSize.Size = new System.Drawing.Size(185, 24);
            this.comboPenSize.TabIndex = 2;
            // 
            // textShapeSize
            // 
            this.textShapeSize.Location = new System.Drawing.Point(820, 90);
            this.textShapeSize.Name = "textShapeSize";
            this.textShapeSize.Size = new System.Drawing.Size(185, 22);
            this.textShapeSize.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1062, 544);
            this.Controls.Add(this.textShapeSize);
            this.Controls.Add(this.comboPenSize);
            this.Controls.Add(this.btnCircle);
            this.Controls.Add(this.btnCanvasColor);
            this.Controls.Add(this.btnRectangle);
            this.Controls.Add(this.btnPenColor);
            this.Controls.Add(this.btnSquare);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.canvasPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel canvasPanel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSquare;
        private System.Windows.Forms.Button btnPenColor;
        private System.Windows.Forms.Button btnRectangle;
        private System.Windows.Forms.Button btnCanvasColor;
        private System.Windows.Forms.Button btnCircle;
        private System.Windows.Forms.ComboBox comboPenSize;
        private System.Windows.Forms.TextBox textShapeSize;
    }
}

