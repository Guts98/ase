﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadsExample
{
   
    class Delegates
    {
        public static void Add(int x, int y)
        {
            Console.WriteLine( x + y);
        }

        public static void Multiply(int x, int y)
        {
            Console.WriteLine(x * y);
        }

        public static void execute()
        {
            // When the methods returned int values 
            //Without multicasting
            /* MyDel del = new MyDel(Add);
            int result = del(10, 30);
            Console.WriteLine(result);
            del = Multiply;            
            result = del(10, 30);
            Console.WriteLine(result); */

            //With multicasting
            /* MyDel del = new MyDel(Add);
             del += Multiply;            //multicast...so first when add is done executingm it will run multiply method
             int result = del(10, 30);
             Console.WriteLine(result); */

            //When the method returns void
            MyDel del = Add;
            del += Multiply;
            del(30,50);
        }

    }
}
