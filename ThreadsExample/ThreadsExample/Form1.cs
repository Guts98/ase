﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ThreadsExample
{
    delegate void MyDel(int x, int y);
    public partial class Form1 : Form
    {
        Thread newThread;
        bool flag = false, running = false;
        public Form1()
        {
            //InitializeComponent();

            //newThread = new Thread(threadMethod);
            // newThread.Start();

           // Delegates d = new Delegates();
            Delegates.execute();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            running = !running;
        }

        public void threadMethod()
        {
            do
            {
                if (!running) continue;
                if (!flag)
                {
                    button1.BackColor = Color.Red;
                    flag = true;
                }
                else
                {
                    button1.BackColor = Color.Gray;
                    flag = false;
                }
                Thread.Sleep(1000);
            } while (true);

        }
    }
}
