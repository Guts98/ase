﻿using System;
using System.Windows.Forms;

namespace lab1
{
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Starting....");
            Program app = new Program();
            app.go();

            MessageBox.Show("Message", "Title",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            Console.WriteLine("Hello World!");

            BMICalculator();

            Console.ReadLine();

        }

        public void go()
        {
            Console.WriteLine("in go");
        }

        static void BMICalculator()
        {


            Console.WriteLine("________________________________BMI CALCULATOR______________________________\n");
            Console.WriteLine("  --------------------------------------------------------------------------\n" +
                              "  | Adults                    |  Women               |  Men                |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | anorexia                  |  < 17.5                                    |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | underweight               | <19.1                |  <20.7              |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | in normal range           | 19.1-25.8            |  20.7-26.4          |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | marginally overweight     | 25.8.27.3            |  26.4-27.8          |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | overweight                | 27.3-32.3            |  27.3-32.3          |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | very overweight or obese  | >32.3                |  31.1               |\n" +
                              "  --------------------------------------------------------------------------\n\n");

            Console.WriteLine("  --------------------------------------------------------------------------\n" +
                              "  | severely obese            | 35-40                                       |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | morbidly obese            | 40-50                                       |\n" +
                              "  --------------------------------------------------------------------------\n" +
                              "  | super obese               | 50-60                                       |\n" +
                              "  --------------------------------------------------------------------------\n");



            //Propmt user input
            Console.WriteLine("\n___________________________________________________________________________");
            string option = "Y";
            do
            {

                Console.WriteLine("Enter your gender (M/F): ");         //Prompt Gender
                string gender = Console.ReadLine().Trim().ToUpper();

                Console.WriteLine("Enter Your Weight in kg: ");         //Prompt weight
                float weight = Single.Parse(Console.ReadLine().Trim());

                Console.WriteLine("Enter your height in meters: ");      //Prompt height
                float height = Single.Parse(Console.ReadLine().Trim());

                float bmi = weight / (height * height);
                Console.WriteLine("Your BMI value is: " + bmi);

                if (gender.Equals("M"))
                {
                    if (bmi < 20.7)
                        Console.WriteLine("You are 'UNDERWEIGHT'!");
                    else if (bmi >= 20.7 && bmi < 26.4)
                    {
                        //MessageBox.Show("You are 'MARGINALLY OVERWEIGHT'!", "BMI"+bmi,
                        //    MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Console.WriteLine("You are 'IN NORMAL RANGE'!");
                    }

                    else if (bmi >= 26.4 && bmi < 27.8)
                        Console.WriteLine("You are 'MARGINALLY OVERWEIGHT'!");
                    else if (bmi >= 27.8 && bmi < 31.1)
                        Console.WriteLine("You are 'OVERWEIGHT'!");
                    else if (bmi >= 31.1 && bmi < 35)
                        Console.WriteLine("You are 'VERY OVERWEIGHT OR OBESE'!");
                    else
                    {
                        if (bmi >= 35 && bmi < 40)
                            Console.WriteLine("You are 'SECERELY OBESE'!");
                        else if (bmi >= 40 && bmi < 50)
                            Console.WriteLine("You are 'MORBIDLY OBESE'!");
                        else if (bmi >= 50 && bmi < 60)
                            Console.WriteLine("You are 'SUPER OBESE'!");
                        else
                            Console.WriteLine("You are a boulder!");
                    }
                }
                else
                {
                    if (bmi < 17.5)
                        Console.WriteLine("You have 'Anorexia'");
                    else if (bmi < 19.1)
                        Console.WriteLine("You are 'UNDERWEIGHT'!");
                    else if (bmi >= 19.1 && bmi < 25.8)
                        Console.WriteLine("You are 'IN NORMAL RANGE'!");
                    else if (bmi >= 25.8 && bmi < 27.3)
                        Console.WriteLine("You are 'MARGINALLY OVERWEIGHT'!");
                    else if (bmi >= 27.3 && bmi < 32.3)
                        Console.WriteLine("You are 'OVERWEIGHT'!");
                    else if (bmi >= 32.3 && bmi < 35)
                        Console.WriteLine("You are 'VERY OVERWEIGHT OR OBESE'!");
                    else
                    {
                        if (bmi >= 35 && bmi < 40)
                            Console.WriteLine("You are 'SECERELY OBESE'!");
                        else if (bmi >= 40 && bmi < 50)
                            Console.WriteLine("You are 'MORBIDLY OBESE'!");
                        else if (bmi >= 50 && bmi < 60)
                            Console.WriteLine("You are 'SUPER OBESE'!");
                        else
                            Console.WriteLine("You are a boulder!");
                    }
                }

                Console.WriteLine("\n_______________________________________________");
                Console.WriteLine("Do you want to continue?<Y/N>: ");
                option = Console.ReadLine().ToUpper().Trim();

            } while (option.Equals("Y"));


            Console.ReadLine();
        }
    }
}
