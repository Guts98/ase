﻿using System;

namespace TestingExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");

            Console.WriteLine("Enter your Name:");
            string name = Console.ReadLine();

            Console.WriteLine("Enter your Age: ");
            int age = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter your Weight: ");
            int weight = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Name: " + name);
            Console.WriteLine("Age: " + age);
            Console.WriteLine("Weight: " + weight);
            Console.WriteLine("Bitbucket version control demo: ");

            //Calling add method from calculator class

            Calculator cal = new Calculator();
            int result = cal.add(6, 7);
            Console.WriteLine("Result:" + result);
        }
    }
}
