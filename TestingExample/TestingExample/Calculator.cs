﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingExample
{
    class Calculator
    {
        public int add(int n1, int n2)
        {
            return n1 + n2;
        }

        public int sub(int n1, int n2)
        {
            return n1 - n2;
        }
    }
}
