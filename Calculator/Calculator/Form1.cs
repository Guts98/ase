﻿using System;
using System.Collections;
//using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        double firstNum = 0;
        double secondNum = 0;
        //double result = 0;
        string operation = "";
        StringBuilder expression = new StringBuilder();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button Clicked!");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnResultHover(object sender, EventArgs e)
        {
            btnResult.BackColor = Color.FromArgb(128, ColorTranslator.FromHtml("#048DAD"));
        }

        private void numNegHover(object sender, EventArgs e)
        {
             //numNeg.BackColor = Color.FromArgb(128, ColorTranslator.FromHtml("#4B4B4B"));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        //private void num0_Click(object sender, EventArgs e)
        //{
        //    setTextOutput("0");
        //}


        //private void numDot_Click(object sender, EventArgs e)
        //{
        //    if (!textOutput.Text.Contains("."))
        //    {
        //        textOutput.Text = textOutput.Text + ".";
        //    }
        //}

        /**
         * Adds the operands to the expression list
        // */
        //private void setExpression(string str)
        //{
        //    expression.Add(str);
        //}


        //private void btnAdd_Click(object sender, EventArgs e)
        //{
        //    if(textOutput.Text != "")
        //    {
        //        setExpression(textOutput.Text);
        //        //operation = "+";
        //        setExpression("+");
        //        setTextExpression();
        //    }
            
            
        //    textOutput.Text = "";
        //}

        private void btnResult_Click(object sender, EventArgs e)
        {
            //setExpression(textOutput.Text);
            //setTextExpression();
            //calculateResult();
            //textOutput.Text = "";
            secondNum = double.Parse(textOutput.Text);
            switch (operation)
            {
                case "+":
                    textOutput.Text = Convert.ToString(firstNum + secondNum);
                    setExpression(Convert.ToString(firstNum), "+", Convert.ToString(secondNum));
                    break;
                case "-":
                    textOutput.Text = Convert.ToString(firstNum - secondNum);
                    setExpression(Convert.ToString(firstNum), "-", Convert.ToString(secondNum));
                    break;
                case "x":
                    textOutput.Text = Convert.ToString(firstNum * secondNum);
                    setExpression(Convert.ToString(firstNum), "*", Convert.ToString(secondNum));
                    break;
                case "÷":
                    textOutput.Text = Convert.ToString(firstNum / secondNum);
                    setExpression(Convert.ToString(firstNum), "/", Convert.ToString(secondNum));
                    break;
                case "%":
                    textOutput.Text = Convert.ToString((firstNum/100) * secondNum);
                    setExpression(Convert.ToString(firstNum), "%", Convert.ToString(secondNum));
                    break;
                default:
                    break;
            }
            expression.Append("=");
            updateTextExpression();
        }

        private void setExpression(String a, String b, String c)
        {
            expression.Append(a);
            if (b.Length > 0)
                expression.Append(b);
            if (c.Length > 0)
                expression.Append(c);

        }

       
        /**
         * Sets the text property of the output text box
         */
        //private void setTextOutput(string num)
        //{
        //    if ((textOutput.Text == "0" && textOutput.Text != null))
        //    {
        //        textOutput.Text = num;
        //    }
        //    else
        //    {
        //        textOutput.Text = textOutput.Text + num;
        //    }
        //}


        private void btnClear_Click(object sender, EventArgs e)
        {
            textOutput.Text = "0";
            textExpression.Text = "";
            expression = new StringBuilder();
            //expression = new ArrayList();
            //expressionText = new StringBuilder();
        }
        private void btnClear2_Click(object sender, EventArgs e)
        {
            textOutput.Text = "0";
            String fn, sn;
            fn = Convert.ToString(firstNum);
            sn = Convert.ToString(secondNum);

            fn = "";
            sn = "";
        }

        private void NumericValue(object sender, EventArgs e)
        {
            Button btns = (Button)sender;

            if (textOutput.Text == "0")
                textOutput.Text = "";

            if(btns.Text == ".")
            {
                if (!textOutput.Text.Contains("."))
                    textOutput.Text = textOutput.Text + btns.Text;
            }
            else
            {
                textOutput.Text = textOutput.Text + btns.Text;
            }
        }

        /**
         * Operational Functions
         */
        private void Operations(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            firstNum = Double.Parse(textOutput.Text);
            operation = btn.Text;
            textOutput.Text = "";
        }

        /**
         * Backspace Function
         */
        private void btnBack_Click(object sender, EventArgs e)
        {
            if(textOutput.Text.Length > 0)
            {
                textOutput.Text = textOutput.Text.Remove(textOutput.Text.Length - 1, 1);
            }

            if (textOutput.Text == "")
            {
                textOutput.Text = "0";
            }
        }

        private void numNeg_Click(object sender, EventArgs e)
        {
            if(textOutput.Text.Contains("-"))
            {
                textOutput.Text = textOutput.Text.Remove(0, 1);
            }
            else
            {
                textOutput.Text = "-" + textOutput.Text;
            }
        }

        private void advancedFunctions(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            operation = btn.Text;
            secondNum = double.Parse(textOutput.Text);
            switch (operation)
            {
                case "√x":
                    textOutput.Text = Convert.ToString(Math.Sqrt(secondNum));
                    setExpression("(√"+Convert.ToString(secondNum)+")","","");
                    break;
                case "1/x":
                    textOutput.Text = Convert.ToString(1 / secondNum);
                    setExpression("(1/"+Convert.ToString(secondNum)+")","","");
                    break;
                case "x2":
                    textOutput.Text = Convert.ToString(secondNum * secondNum);
                    setExpression("("+Convert.ToString(secondNum)+"^2)","","");
                    break;
                default:
                    break;
            }
            expression.Append("=");
            updateTextExpression();
        }

        private void updateTextExpression()
        {
            textExpression.Text = Convert.ToString(expression);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
