﻿//https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/serialization/walkthrough-persisting-an-object-in-visual-studio
//potential prblems. If you change the source then the output file will automatically raise an exception when you try and deserialize it (delete it to recreate).
//I've added TestClass.cs and then put a reference to it in the serializable Loan.cs. If you don't make it as NotSerizlizable you'll get an exception as the TestClass itself is not serializable.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SerializeExample
{
    class Program
    {
        const string FileName = @"../../../SavedLoan.bin";
        static void Main(string[] args)
        {
            Loan TestLoan = new Loan(10000.0, 7.5, 36, "Neil Black");

           if (File.Exists(FileName))
            {
                Console.WriteLine("Reading saved file");
                Stream openFileStream = File.OpenRead(FileName);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter deserializer = new BinaryFormatter();
                TestLoan = (Loan)deserializer.Deserialize(openFileStream);
                TestLoan.TimeLastLoaded = DateTime.Now;
                openFileStream.Close();
            }

    
            TestLoan.PropertyChanged += (_, __) => Console.WriteLine($"New customer value: {TestLoan.Customer}");

            TestLoan.Customer = "Henry Clay";
            Console.WriteLine(TestLoan.InterestRatePercent);
            TestLoan.InterestRatePercent = 7.1;
            Console.WriteLine(TestLoan.InterestRatePercent);

            Stream SaveFileStream = File.Create(FileName);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(SaveFileStream, TestLoan);
            SaveFileStream.Close();
            Console.ReadKey();

        }
    }
}
